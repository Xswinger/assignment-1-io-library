section .text
 
%define EXIT_SYSCALL 60
%define A_HEX 10
%define SPACE 32 
%define TAB 9
%define NEW_STRING 10

 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_SYSCALL
    xor rdi, rdi
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.count:
    cmp byte [rdi+rax], 0 
    je .end
    inc rax
    jmp .count  
.end:
    ret  

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, A_HEX
    call print_char
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov r8, rdi
    test rdi, rdi ; set pointers
    jns print_uint
    mov rdi, '-'
    call print_char ; print '-' if less zero
    mov rdi, r8
    neg rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    push 0
    mov r10, 10
.loop: ; division and remainder loop
    mov rdx, 0
    div r10
    add rdx, '0'
    push rdx
    cmp rax, r10
    jae .loop
    add rax, '0'
    cmp rax, '0'
    je .next_num
    push rax
.next_num: ; next number
    pop rdi
    cmp rdi, 0
    je .return
    call print_char
    jmp .next_num
.return:
    ret
    

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
.loop:
    mov al, byte[rdi+rcx]
    mov dl, byte[rsi+rcx]
    cmp al, dl
    jne .not_equals
    inc rcx
    cmp al, 0
    je .equals
    jmp .loop
.equals:
    mov rax, 1
    ret
.not_equals:
    xor rax, rax
    ret    

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    push 0
    mov rsi, rsp
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rdi
    push rsi
    mov rcx, rsi
.read_new_char:
    push rdi
    push rcx
    call read_char
    pop rcx
    pop rdi

    ; check for space simbols
    cmp al, TAB
    je .read_new_char
    cmp al, NEW_STRING
    je .read_new_char
    cmp al, SPACE
    je .read_new_char
.loop:
    cmp rcx, 1 ; if buffer size is 1
    jle .error
    cmp rax, 0 ; if next symbol is null-terminator
    jle .end
    mov byte[rdi], al
    inc rdi
    dec rcx ; decrease buffer free size

    push rdi
    push rcx
    call read_char
    pop rcx
    pop rdi
.check_space:
    cmp al, TAB
    je .end
    cmp al, NEW_STRING
    je .end
    cmp al, SPACE
    je .end
    jmp .loop
.end:
    pop rdx
    pop rax
    sub rdx, rcx
    mov byte[rdi], 0
    ret
.error:
    pop rdx
    pop rax
    xor rax, rax
    ret    

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    xor rdx, rdx
.loop:
    mov dl, byte[rdi+rcx]
    sub dl, 48
    cmp dl, 0 ; check if string is '0'
    js .out
    cmp dl, 10 ; check if string is '\n'
    jns .out
    imul rax, 10 ; multiplying a number by 10
    add rax, rdx
    inc rcx
    jmp .loop
    ret
.out:
    mov rdx, rcx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jne .is_int
.uint: ; if unsigned
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret
.is_int: ; signed
    call parse_uint
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push r8
    call string_length
    cmp rdx, rax ; buffer capacity check
    jl .error
.loop:
    mov r8, 0
    mov r8b, byte[rdi]
    mov byte[rsi], r8b
    cmp r8, 0 ; check null-terminator
    je .end
    inc rdi
    inc rsi
    jmp .loop
.error:
    mov rax, 0
.end:
    pop r8
    ret
